from django.shortcuts import render
from todos.models import TodoList, TodoItem


# Create your views here.
def to_do_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }

    return render(request, "todos/list.html", context)
