from django.urls import path, include
from todos.views import todo_list_list, todo_list_detail

urlpatterns = [
    path("todos/", todo_list_list, name="todo_list_list"),
    path("int:id>", todo_list_detail, name="todo_list_detail"),
]
